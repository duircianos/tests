<?php

error_reporting(-1);
ini_set('display_errors', 'on');

require_once 'vendor/autoload.php';

use app\Query;

session_start();

/*

Кусок кода для MVC подходов (заготовка для других ТЗ)
$routes = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

if (!empty($routes[0])) {
    $route = strstr($routes[0], '?', true);
    $controllerName = str_replace('_', '', ucwords($route ?: $routes[0], '_')) . 'Controller';


    $controllerName = 'app\controllers\\' . $controllerName;

    if (class_exists($controllerName)) {

        $controller = new $controllerName();

        if (!empty($routes[1])) {
            $controller->{$routes[1]}();
        } else {
            $controller->getView();
        }
    } else {
        echo 'Такого пути не существует';
        exit;
    }
} else {
    $controller = new Query();
    $controller->fetch();
}

*/

$controller = new Query();
$controller->fetch();