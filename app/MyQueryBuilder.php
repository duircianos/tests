<?php

namespace app;

use PDO;


class MyQueryBuilder
{
    private $dbh;
    public $select;
    public $from;
    private $table;
    public $where = 'WHERE 1' . PHP_EOL;
    public $prepare;
    public $limit = 'LIMIT 10';

    public function __construct($host, $dbname, $user, $pass)
    {
        $this->dbh = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);

        return $this;
    }

    public function select($select = "*")
    {
        if (is_array($select))
            $select = implode(',', $select);

        $this->select = "SELECT $select" . PHP_EOL;

        return $this;
    }

    public function from($table)
    {
        $this->table = $table;

        $this->from = "FROM $table" . PHP_EOL;

        return $this;
    }

    public function where($field, $equal, $value)
    {
        $this->where .= "AND $field $equal '" . "$value'" . PHP_EOL;

        return $this;
    }

    public function limit($count)
    {
        $this->limit = "LIMIT $count" . PHP_EOL;

        return $this;
    }

    public function insert($insert)
    {
        $query = "INSERT INTO " . $this->table;

        $query .= '('.implode(',', $insert[0]).')';

        $query .= ' VALUES';

        $query .= '(';

        foreach ($insert[1] as $value)
            $query .= "'".$value."',";

        $query = substr($query, 0, -1);

        $query .= ')';

        $stn = $this->dbh->prepare($query);
        $stn->execute();

        return 'success';
    }

    public function update($sets)
    {
        $query = "UPDATE " . $this->table . PHP_EOL . 'SET ';

        foreach ($sets as $set)
            $query .= $set[0] . ' = ' . '"'.$set[1].'"' . PHP_EOL;

        $query .= $this->where;
        $stn = $this->dbh->prepare($query);
        $stn->execute();

        return 'success';
    }

    public function delete($id)
    {
        $query = "DELETE " . $this->from . 'WHERE id = ' . $id;
        $stn = $this->dbh->prepare($query);
        $stn->execute();

        return 'success';
    }

    public function execute()
    {
        $query = $this->select . $this->from . $this->where . $this->limit;
        $stn = $this->dbh->prepare($query);
        $stn->execute();
        $result = $stn->fetchAll();
        return $result;
    }
}